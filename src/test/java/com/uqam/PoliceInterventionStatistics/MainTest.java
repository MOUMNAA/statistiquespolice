package com.uqam.PoliceInterventionStatistics;

import com.uqam.PoliceInterventionStatistics.service.CSVFileProcessor;
import java.io.File;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class MainTest {

  @Test
  @DisplayName("C1")
  public void testEmptyEntryOutPath() {
    CSVFileProcessor csvFileProcessor = new CSVFileProcessor();
    String args[] = {"", ""};
    String expectedMessage = "le path pour le fichier d'entree est vide";
    Throwable thrown =
        assertThrows(Exception.class, () -> csvFileProcessor.startStatistcsProcessing(args));
    assertEquals(expectedMessage, thrown.getMessage());
  }

  @Test
  @DisplayName("C2")
  public void testNoParams() {
    CSVFileProcessor csvFileProcessor = new CSVFileProcessor();
    String args[] = {};
    String expectedMessage = "le nombre de parametres est incorrecte";
    Throwable thrown =
        assertThrows(Exception.class, () -> csvFileProcessor.startStatistcsProcessing(args));
    assertEquals(expectedMessage, thrown.getMessage());
  }

  @Test
  @DisplayName("C3")
  public void testMoreThanTwoPrams() {
    CSVFileProcessor csvFileProcessor = new CSVFileProcessor();
    String args[] = {"/test1.csv", "/test2.csv", "test3.csv", "tp1.2_invalid_delimiter_file.csv"};
    String expectedMessage = "le nombre de parametres est incorrecte";
    Throwable thrown =
        assertThrows(Exception.class, () -> csvFileProcessor.startStatistcsProcessing(args));
    assertEquals(expectedMessage, thrown.getMessage());
  }

  @Test
  @DisplayName("C4")
  public void testLessThanTwoPrams() {
    CSVFileProcessor csvFileProcessor = new CSVFileProcessor();
    String args[] = {"/test1.csv"};
    String expectedMessage = "le nombre de parametres est incorrecte";
    Throwable thrown =
        assertThrows(Exception.class, () -> csvFileProcessor.startStatistcsProcessing(args));
    assertEquals(expectedMessage, thrown.getMessage());
  }

  @Test
  @DisplayName("C5")
  public void testNotFoundEntryFile() {
    CSVFileProcessor csvFileProcessor = new CSVFileProcessor();
    String args[] = {"/test.csv", "/test2.csv"};
    String expectedMessage = "fichier d'entree est introuvable";
    Throwable thrown =
        assertThrows(Exception.class, () -> csvFileProcessor.startStatistcsProcessing(args));
    assertEquals(expectedMessage, thrown.getMessage());
  }

  @Test
  @DisplayName("C6")
  public void testSamePrams() {
    CSVFileProcessor csvFileProcessor = new CSVFileProcessor();
    String args[] = {"/test.csv", "/test.csv"};
    String expectedMessage = "le chemin d'entree doit etre different de la sortie";
    Throwable thrown =
        assertThrows(Exception.class, () -> csvFileProcessor.startStatistcsProcessing(args));
    assertEquals(expectedMessage, thrown.getMessage());
  }

  @Test
  @DisplayName("C7")
  public void testInvalidEntryFile() {
    CSVFileProcessor csvFileProcessor = new CSVFileProcessor();
    String args[] = {"src/test/resources/tp1.2_invalid_delimiter_file.csv", "/test2.csv"};
    String expectedMessage = "le format de fichier d'entree est invalid";
    Throwable thrown =
        assertThrows(Exception.class, () -> csvFileProcessor.startStatistcsProcessing(args));
    assertEquals(expectedMessage, thrown.getMessage());
  }

  @Test
  @DisplayName("C8")
  public void testEmptyEntryFile() {
    File file = new File("src/test/resources/tp1.3_empty_file.csv");
    String entryPath = file.getAbsolutePath();
    CSVFileProcessor csvFileProcessor = new CSVFileProcessor();
    String args[] = {entryPath, "/test2.csv"};
    String expectedMessage = "le fichier d'entree est vide";
    Throwable thrown =
        assertThrows(Exception.class, () -> csvFileProcessor.startStatistcsProcessing(args));
    assertEquals(expectedMessage, thrown.getMessage());
  }

  @Test
  @DisplayName("C9")
  public void testHeaderDateIvalid() {
    File file = new File("src/test/resources/tp1.4_header_date.csv");
    String entryPath = file.getAbsolutePath();
    CSVFileProcessor csvFileProcessor = new CSVFileProcessor();
    String args[] = {entryPath, "/test2.csv"};
    String expectedMessage =
        "Erreur dans le fichier tp1.4_header_date.csv a la ligne 0 "
            + ": Le champ 'Date' est manquant.";
    Throwable thrown =
        assertThrows(Exception.class, () -> csvFileProcessor.startStatistcsProcessing(args));
    assertEquals(expectedMessage, thrown.getMessage());
  }

  @Test
  @DisplayName("C10")
  public void testHeaderHourIvalid() {
    File file = new File("src/test/resources/tp1.4_header_hour.csv");
    String entryPath = file.getAbsolutePath();
    CSVFileProcessor csvFileProcessor = new CSVFileProcessor();
    String args[] = {entryPath, "/test2.csv"};
    String expectedMessage =
        "Erreur dans le fichier tp1.4_header_hour.csv a la ligne"
            + " 0 : Le champ 'Heure' est manquant.";
    Throwable thrown =
        assertThrows(Exception.class, () -> csvFileProcessor.startStatistcsProcessing(args));
    assertEquals(expectedMessage, thrown.getMessage());
  }

  @Test
  @DisplayName("C11")
  public void testHeaderParcIvalid() {
    File file = new File("src/test/resources/tp1.4_header_parc.csv");
    String entryPath = file.getAbsolutePath();
    CSVFileProcessor csvFileProcessor = new CSVFileProcessor();
    String args[] = {entryPath, "/test2.csv"};
    String expectedMessage =
        "Erreur dans le fichier tp1.4_header_parc.csv a "
            + "la ligne 0 : Le champ 'Parc' est manquant.";
    Throwable thrown =
        assertThrows(Exception.class, () -> csvFileProcessor.startStatistcsProcessing(args));
    assertEquals(expectedMessage, thrown.getMessage());
  }

  @Test
  @DisplayName("C12")
  public void testHeaderArrondissementIvalid() {
    File file = new File("src/test/resources/tp1.4_header_arrondissement.csv");
    String entryPath = file.getAbsolutePath();
    CSVFileProcessor csvFileProcessor = new CSVFileProcessor();
    String args[] = {entryPath, "/test2.csv"};
    String expectedMessage =
        "Erreur dans le fichier tp1.4_header_arrondissement.csv a la ligne 0 :"
            + " Le champ 'Arrondissement' est manquant.";
    Throwable thrown =
        assertThrows(Exception.class, () -> csvFileProcessor.startStatistcsProcessing(args));
    assertEquals(expectedMessage, thrown.getMessage());
  }

  @Test
  @DisplayName("C13")
  public void testHeaderDescriptionIvalid() {
    File file = new File("src/test/resources/tp1.4_header_description.csv");
    String entryPath = file.getAbsolutePath();
    CSVFileProcessor csvFileProcessor = new CSVFileProcessor();
    String args[] = {entryPath, "/test2.csv"};
    String expectedMessage =
        "Erreur dans le fichier tp1.4_header_description.csv a "
            + "la ligne 0 : Le champ 'Description' est manquant.";
    Throwable thrown =
        assertThrows(Exception.class, () -> csvFileProcessor.startStatistcsProcessing(args));
    assertEquals(expectedMessage, thrown.getMessage());
  }

  @Test
  @DisplayName("C14")
  public void testInvalidDate() {
    File file = new File("src/test/resources/tp1.6_invalid_hour.csv");
    String entryPath = file.getAbsolutePath();
    CSVFileProcessor csvFileProcessor = new CSVFileProcessor();
    String args[] = {entryPath, "/test2.csv"};
    String expectedMessage =
        "Erreur dans le fichier tp1.6_invalid_hour.csv a "
            + "la ligne 2 : la valeur de champ 'Heure' est invalide.";
    Throwable thrown =
        assertThrows(Exception.class, () -> csvFileProcessor.startStatistcsProcessing(args));
    assertEquals(expectedMessage, thrown.getMessage());
  }

  @Test
  @DisplayName("C15")
  public void testInvalidHours() {
    File file = new File("src/test/resources/tp1.6_invalid_hour.csv");
    String entryPath = file.getAbsolutePath();
    CSVFileProcessor csvFileProcessor = new CSVFileProcessor();
    String args[] = {entryPath, "/test2.csv"};
    String expectedMessage =
        "Erreur dans le fichier tp1.6_invalid_hour.csv a la ligne 2 "
            + ": la valeur de champ 'Heure' est invalide.";
    Throwable thrown =
        assertThrows(Exception.class, () -> csvFileProcessor.startStatistcsProcessing(args));
    assertEquals(expectedMessage, thrown.getMessage());
  }

  @Test
  @DisplayName("C16")
  public void testEmptyColoumnEntryFile() {
    File file = new File("src/test/resources/tp1.7_empty_column.csv");
    String entryPath = file.getAbsolutePath();
    CSVFileProcessor csvFileProcessor = new CSVFileProcessor();
    String args[] = {entryPath, "/test2.csv"};
    String expectedMessage =
        "Erreur dans le fichier tp1.7_empty_column.csv a la ligne 2 "
            + ": Le champ 'Description' est manquant.";
    Throwable thrown =
        assertThrows(Exception.class, () -> csvFileProcessor.startStatistcsProcessing(args));
    assertEquals(expectedMessage, thrown.getMessage());
  }

  @Test
  @DisplayName("C17")
  public void testInvalidEntryFileArrondissement() {
    File file = new File("src/test/resources/tp1.8_invalid_arrondissement.csv");
    String entryPath = file.getAbsolutePath();
    CSVFileProcessor csvFileProcessor = new CSVFileProcessor();
    String args[] = {entryPath, "/test2.csv"};
    String expectedMessage =
        "Erreur dans le fichier tp1.8_invalid_arrondissement.csv a la ligne 2 : "
            + "Le champ 'Arrondissement' non trouvé sur le fichier json.";
    Throwable thrown =
        assertThrows(Exception.class, () -> csvFileProcessor.startStatistcsProcessing(args));
    assertEquals(expectedMessage, thrown.getMessage());
  }

  @Test
  @DisplayName("C18")
  public void testInvalidEntryFileDescription() {
    File file = new File("src/test/resources/tp1.9_invalid_description.csv");
    String entryPath = file.getAbsolutePath();
    CSVFileProcessor csvFileProcessor = new CSVFileProcessor();
    String args[] = {entryPath, "/test2.csv"};
    String expectedMessage =
        "Erreur dans le fichier tp1.9_invalid_description.csv a la ligne 2 : "
            + "Le champ 'Description' non trouvé sur le fichier json.";
    Throwable thrown =
        assertThrows(Exception.class, () -> csvFileProcessor.startStatistcsProcessing(args));
    assertEquals(expectedMessage, thrown.getMessage());
  }

  @Test
  @DisplayName("C19")
  public void testValidEntryFile() throws Exception {
    File entryFile = new File("src/test/resources/tp1.10_valid_file.csv");
    String entryPath = entryFile.getAbsolutePath();
    String outPath = "/test/sortie.csv";
    CSVFileProcessor csvFileProcessor = new CSVFileProcessor();
    String args[] = {entryPath, outPath};
    csvFileProcessor.startStatistcsProcessing(args);
    File outFile = new File(outPath);
    assertTrue(outFile.exists());
  }
}
