package com.uqam.PoliceInterventionStatistics.model;

/**
 * Cette classe modelise une statistique.
 *
 * @author
 * @version
 */
public class Statistique {

  String arrondissement;
  int nombre;

  /**
   * cette methode retourne le nom de l'arrondissement
   *
   * @return le nom de l'arrondissement
   */
  public String getArrondissement() {
    return arrondissement;
  }

  /**
   * modifie l'arrondissement de cette statistique
   *
   * @param arrondissement le nom de l'arrondissement de cette statistique
   */
  public void setArrondissement(String arrondissement) {
    this.arrondissement = arrondissement;
  }

  /**
   * cette methode retourne le nombre des arrondissement trouves
   *
   * @return le nombre des arrondissement trouves
   */
  public int getNombre() {
    return nombre;
  }

  /**
   * modifie le nombre des arrondissement de cette statistique
   *
   * @param nombre le nombre des arrondissement de cette statistique
   */
  public void setNombre(int nombre) {
    this.nombre = nombre;
  }
}
