package com.uqam.PoliceInterventionStatistics.model;

import java.time.LocalDate;

/**
 * Cette classe modelise une Intervention.
 *
 * @author
 * @version
 */
public class Intervention {

  LocalDate date;
  String hour;
  String park;
  String arrondissement;
  String description;

  /**
   * cette methode retourne la date de l'Intervention
   *
   * @return la date de l'Intervention
   */
  public LocalDate getDate() {
    return date;
  }

  /**
   * modifie la date de l'Intervention
   *
   * @param date la date de l'Intervention
   */
  public void setDate(LocalDate date) {
    this.date = date;
  }

  /**
   * cette methode retourne l'heure de l'Intervention
   *
   * @return l'heure de l'Intervention
   */
  public String getHour() {
    return hour;
  }

  /**
   * modifie l'heure de l'Intervention
   *
   * @param hour l'heure de l'Intervention
   */
  public void setHour(String hour) {
    this.hour = hour;
  }

  /**
   * cette methode retourne le park de l'Intervention
   *
   * @return le park de l'Intervention
   */
  public String getPark() {
    return park;
  }

  /**
   * modifie le park de l'Intervention
   *
   * @param park le park de l'Intervention
   */
  public void setPark(String park) {
    this.park = park;
  }

  /**
   * cette methode retourne le nom de l'arrondissement
   *
   * @return le nom de l'arrondissement
   */
  public String getArrondissement() {
    return arrondissement;
  }

  /**
   * modifie le nom de l'arrondissement
   *
   * @param arrondissement le nom de l'arrondissement
   */
  public void setArrondissement(String arrondissement) {
    this.arrondissement = arrondissement;
  }

  /**
   * cette methode retourne la description de l'Intervention
   *
   * @return la description de l'Intervention
   */
  public String getDescription() {
    return description;
  }

  /**
   * modifie la description de l'Intervention
   *
   * @param description la description de l'Intervention
   */
  public void setDescription(String description) {
    this.description = description;
  }
}
