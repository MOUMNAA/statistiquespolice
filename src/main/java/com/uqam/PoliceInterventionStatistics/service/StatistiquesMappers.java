package com.uqam.PoliceInterventionStatistics.service;

import com.uqam.PoliceInterventionStatistics.model.Intervention;
import com.uqam.PoliceInterventionStatistics.model.Statistique;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

/**
 * Cette classe contiens les transformateurs utilisés pour le mapping des objets Pour des raisons
 * d'organisation et de tests ces transformateurs sont centralisés au niveau de cette classe
 */
public class StatistiquesMappers {

  /**
   * cette fonction retourne une liste des valeurs par clé sur le fichier json
   *
   * @param fileName le nom de fichier json d'entree
   * @param key clé de recherche
   * @return liste des valeurs
   * @throws IOException si le fichier json est introuvable
   */
  private static List<String> JSONFileToList(String fileName, String key)
      throws IOException, ClassNotFoundException {

    Class cls = Class.forName("com.uqam.PoliceInterventionStatistics.service.StatistiquesMappers");

    ClassLoader cLoader = cls.getClassLoader();

    String path = String.format("json/%s.json", fileName);
    InputStream i = cLoader.getResourceAsStream(path);
    BufferedReader reader = new BufferedReader(new InputStreamReader(i));
    StringBuffer sb = new StringBuffer();
    String str = "";
    while ((str = reader.readLine()) != null) {
      sb.append(str);
    }
    String jsonFileTxt = sb.toString();

    JSONObject jsonObject = (JSONObject) JSONSerializer.toJSON(jsonFileTxt);
    JSONArray arrondissementsArray = (JSONArray) jsonObject.get(key);
    Iterator<String> arrondissementsIterator = arrondissementsArray.iterator();
    List<String> arrondissementsList = new ArrayList<>();
    arrondissementsIterator.forEachRemaining(arrondissementsList::add);
    return arrondissementsList;
  }

  /**
   * cette fonction transforme une ligne de fichier CSV d'entree en objet Intervention
   *
   * @param lineNumber le numero de ligne sur fichier CSV d'entree
   * @param csvLine liste des valuers de la ligne de fichier CSV
   * @param fileName le nom de fichier CSV d'entree
   * @return insatnce de classe Intervention
   * @throws Exception si l'une des colonnes de fichier CSV d'entree est non valide
   */
  public static Intervention mapLineCSVToIntervention(
      int lineNumber, List<String> csvLine, String fileName) throws Exception {

    Intervention intervention = new Intervention();

    if (!Validators.isValidData(csvLine.get(0))) {
      throw new Exception(Validators.buildErrorMessageColumnNotFound(lineNumber, "Date", fileName));
    } else if (!Validators.isValidDate(csvLine.get(0))) {
      throw new Exception(Validators.buildErrorMessageInvalidData(lineNumber, "Date", fileName));
    } else {
      intervention.setDate(LocalDate.parse(csvLine.get(0)));
    }

    if (!Validators.isValidData(csvLine.get(1))) {
      throw new Exception(
          Validators.buildErrorMessageColumnNotFound(lineNumber, "Heure", fileName));
    } else if (!Validators.isValidHeure(csvLine.get(1))) {
      throw new Exception(Validators.buildErrorMessageInvalidData(lineNumber, "Heure", fileName));
    } else {
      intervention.setHour(csvLine.get(1));
    }
    if (!Validators.isValidData(csvLine.get(2))) {
      throw new Exception(Validators.buildErrorMessageColumnNotFound(lineNumber, "Parc", fileName));
    } else {
      intervention.setPark(csvLine.get(2));
    }
    if (!Validators.isValidData(csvLine.get(3))) {
      throw new Exception(
          Validators.buildErrorMessageColumnNotFound(lineNumber, "Arrondissement", fileName));
    } else if (!JSONFileToList("arrondissements", "arrondissements").contains(csvLine.get(3))) {
      throw new Exception(
          Validators.buildErrorMessageDataNotFoundInJSON(lineNumber, "Arrondissement", fileName));
    } else {
      intervention.setArrondissement(csvLine.get(3));
    }
    if (!Validators.isValidData(csvLine.get(4))) {
      throw new Exception(
          Validators.buildErrorMessageColumnNotFound(lineNumber, "Description", fileName));
    } else if (!JSONFileToList("interventions", "intervention_policiere")
        .contains(csvLine.get(4))) {
      throw new Exception(
          Validators.buildErrorMessageDataNotFoundInJSON(lineNumber, "Description", fileName));
    } else {
      intervention.setDescription(csvLine.get(4));
    }
    return intervention;
  }

  /**
   * cette fonction retourne une liste des Statistiques a partir d'une
   * map<nom_arrondissement,List<Intervention>>
   *
   * @param arrondissementMap map <nom_arrondissement,List<Intervention>>
   * @return liste des Statistiques
   */
  public static List<Statistique> mapArrondissementMapToListStatistique(
      Map<String, List<Intervention>> arrondissementMap) {
    List<Statistique> statistiques = new ArrayList<>();
    for (Map.Entry<String, List<Intervention>> entry : arrondissementMap.entrySet()) {
      Statistique statistique = new Statistique();
      statistique.setArrondissement(entry.getKey());
      statistique.setNombre(entry.getValue().size());
      statistiques.add(statistique);
    }
    return statistiques;
  }

  /**
   * cette fonction decoupe une ligne de ficher CSV en liste des valeurs
   *
   * @param line une ligne de ficher CSV
   * @return liste des valeurs
   */
  public static List<String> mapCSVLineToList(String line) {

    List<String> values = new ArrayList<>();
    try (Scanner rowScanner = new Scanner(line)) {
      rowScanner.useDelimiter(",");
      while (rowScanner.hasNext()) {
        values.add(rowScanner.next());
      }
    }
    int lineSize = values.size();
    if (lineSize < 5) {
      for (int i = lineSize; i < 6; i++) {
        values.add(i, "");
      }
    }
    return values;
  }
}
