package com.uqam.PoliceInterventionStatistics.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.List;

/**
 * Cette classe contiens les validateurs utilisés pour la validation des données Pour des raisons
 * d'organisation et de tests ces validateurs sont centralisés au niveau de cette classe
 */
public class Validators {

  /**
   * cette méthode verifie la validité de la date passée en entrée
   *
   * @param dateStr la date a verifier
   * @return true si la date est valide,false sinon
   */
  public static boolean isValidDate(String dateStr) {
    try {
      LocalDate.parse(dateStr, DateTimeFormatter.ISO_DATE);
    } catch (DateTimeParseException e) {
      return false;
    }
    return true;
  }

  /**
   * cette méthode verifie la validité de l'heure passée en entrée
   *
   * @param hour l'heure a verifier
   * @return true si l'heure est valide,false sinon
   */
  public static boolean isValidHeure(String hour) {
    try {
      DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
      LocalDate.parse("12-12-2017 " + hour, formatter);
    } catch (DateTimeParseException e) {
      return false;
    }
    return true;
  }

  /**
   * cette méthode verifie la validité de la donnée passée en entrée
   *
   * @param data la donnée a verifier
   * @return true si l'heure est valide,false sinon
   */
  public static boolean isValidData(String data) {
    return data != null && !data.trim().isEmpty() && !data.isEmpty();
  }

  /**
   * cette fonction permet de construire un message d'erreur en cas de colonne non trouvée
   *
   * @param lineNumber le numero de line erronée
   * @param fieldName le nom de la colonne
   * @param fileName le nom de fichier CSV d'entree
   * @return chaine de caracteres de l'exception
   */
  public static String buildErrorMessageColumnNotFound(
      int lineNumber, String fieldName, String fileName) {
    return String.format(
        "Erreur dans le fichier %s a la ligne %s : Le champ '%s' est manquant.",
        fileName, lineNumber, fieldName);
  }

  /**
   * cette fonction permet de construire un message d'erreur en cas de mot non trouvée sur le
   * fichier CSV d'entree
   *
   * @param lineNumber le numero de line erronée
   * @param fieldName le nom de la colonne
   * @param fileName le nom de fichier CSV d'entree
   * @return chaine de caracteres de l'exception
   */
  public static String buildErrorMessageDataNotFoundInJSON(
      int lineNumber, String fieldName, String fileName) {
    return String.format(
        "Erreur dans le fichier %s a la ligne %s : Le champ '%s' non trouvé sur le fichier json.",
        fileName, lineNumber, fieldName);
  }

  /**
   * cette fonction permet de construire un message d'erreur en cas de mot non valid sur le fichier
   * CSV d'entree
   *
   * @param lineNumber le numero de line erronée
   * @param fieldName le nom de la colonne
   * @param fileName le nom de fichier CSV d'entree
   * @return chaine de caracteres de l'exception
   */
  public static String buildErrorMessageInvalidData(
      int lineNumber, String fieldName, String fileName) {
    return String.format(
        "Erreur dans le fichier %s a la ligne %s : la valeur de champ '%s' est invalide.",
        fileName, lineNumber, fieldName);
  }

  /**
   * cette fonction permet de construire un message d'erreur en cas d'entete non valid sur le
   * fichier * CSV d'entree
   *
   * @param csvHeader liste des valeurs de l'entete
   * @param fileName le nom de fichier CSV d'entree
   * @throws Exception si la valeur de la colonne d'entete est nulle ou vide
   */
  public static void validateHeader(List<String> csvHeader, String fileName) throws Exception {

    List<String> header = Arrays.asList("Date", "Heure", "Parc", "Arrondissement", "Description");
    if (!header.get(0).equals(csvHeader.get(0))) {
      throw new Exception(buildErrorMessageColumnNotFound(0, "Date", fileName));
    } else if (!header.get(1).equals(csvHeader.get(1))) {
      throw new Exception(buildErrorMessageColumnNotFound(0, "Heure", fileName));
    } else if (!header.get(2).equals(csvHeader.get(2))) {
      throw new Exception(buildErrorMessageColumnNotFound(0, "Parc", fileName));
    } else if (!header.get(3).equals(csvHeader.get(3))) {
      throw new Exception(buildErrorMessageColumnNotFound(0, "Arrondissement", fileName));
    } else if (!header.get(4).equals(csvHeader.get(4))) {
      throw new Exception(buildErrorMessageColumnNotFound(0, "Description", fileName));
    }
  }
}
