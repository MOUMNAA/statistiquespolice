package com.uqam.PoliceInterventionStatistics.service;

import com.opencsv.CSVWriter;
import com.univocity.parsers.csv.CsvFormat;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import com.uqam.PoliceInterventionStatistics.model.Intervention;
import com.uqam.PoliceInterventionStatistics.model.Statistique;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * cette classe regroupe les differentes methodes utilisées pour le traitement de fichier CSV
 * d'entree
 */
public class CSVFileProcessor {

  /**
   * cette fonction permet de retourner la liste des lignes de fichier CSV dont le chemine est passé
   * en param
   *
   * @param entryPath : chemin de fichier CSV
   * @return la liste des lignes de fichier CSV
   * @throws IOException si le fichier passé en param est introuvable
   * @throws Exception si le fichier passé en param est vide
   */
  private List<List<String>> readFile(String entryPath) throws Exception {

    File entryFile = new File(entryPath);
    if (!entryFile.exists()) {
      throw new IOException("fichier d'entree est introuvable");
    }

    CsvParserSettings settings = new CsvParserSettings();
    settings.detectFormatAutomatically();
    CsvParser parser = new CsvParser(settings);
    parser.parseAll(new File(entryPath));
    CsvFormat format = parser.getDetectedFormat();
    if (format.getDelimiter() != ',') {
      throw new Exception("le format de fichier d'entree est invalid");
    }

    Scanner scanner = new Scanner(entryFile);
    List<List<String>> csvLines = new ArrayList<>();
    while (scanner.hasNextLine()) {
      csvLines.add(StatistiquesMappers.mapCSVLineToList(scanner.nextLine()));
    }
    if (csvLines.size() < 2) {
      throw new Exception("le fichier d'entree est vide");
    }

    List<String> csvHeader = csvLines.get(0);
    Validators.validateHeader(csvHeader, entryFile.getName());

    return csvLines;
  }

  /**
   * cette fonction permet de calculer la liste des Statistiques a partir de la liste des lignes de
   * fichier CSV passée en param
   *
   * @param csvLines liste des lignes de fichier CSV
   * @param entryFileName nom de fichier CSV
   * @return la liste des Statistiques
   * @throws Exception si la fonction mapListLineCSVToListIntervention lève une exception
   */
  private List<Statistique> processCSVFiles(List<List<String>> csvLines, String entryFileName)
      throws Exception {
    List<Intervention> interventions = mapListLineCSVToListIntervention(csvLines, entryFileName);
    Map<String, List<Intervention>> mp =
        interventions.stream().collect(Collectors.groupingBy(Intervention::getArrondissement));
    return StatistiquesMappers.mapArrondissementMapToListStatistique(mp);
  }

  /**
   * cette fonction permet de transformer la liste des lignes de fichier CSV passée en param en
   * liste des Interventions
   *
   * @param csvLines la liste des lignes de fichier CSV
   * @param fileName nom de fichier CSV
   * @return liste des Interventions
   * @throws Exception si la fonction mapLineCSVToIntervention lève une exception
   */
  private List<Intervention> mapListLineCSVToListIntervention(
      List<List<String>> csvLines, String fileName) throws Exception {
    List<Intervention> interventions = new ArrayList<>();
    for (int lineNumber = 1; lineNumber < csvLines.size(); lineNumber++) {
      interventions.add(
          StatistiquesMappers.mapLineCSVToIntervention(
              lineNumber + 1, csvLines.get(lineNumber), fileName));
    }
    return interventions;
  }

  /**
   * cette fonction permet d'ecrir la liste des statistiques sur le fichier CSV de sortie dont le
   * chemin est passé en param
   *
   * @param statistiques la liste des statistiques
   * @param outPath le chemin de fichier CSV de sortie
   * @throws IOException si le chemin d'ecrirture est introuvable
   */
  private void writeCSVFile(List<Statistique> statistiques, String outPath) throws IOException {
    File file = new File(outPath);
    if (!file.exists()) {
      file.getParentFile().mkdirs();
      file.createNewFile();
    }
    FileWriter fileWriter = new FileWriter(outPath);
    CSVWriter csvWriter = new CSVWriter(fileWriter, ',', CSVWriter.NO_QUOTE_CHARACTER);
    String[] header = {"Arrondissement", "Nombre d'interventions"};
    csvWriter.writeNext(header);
    for (Statistique statistique : statistiques) {
      String line[] = {statistique.getArrondissement(), String.valueOf(statistique.getNombre())};
      csvWriter.writeNext(line);
    }
    csvWriter.flush();
  }

  /**
   * Cette methode regroupe les traitements de lecture,calcul,generation des statistiques
   * @param args parametres de programme
   * @throws Exception si les parametres ou le fichier d'entree est invalid
   */
  public void startStatistcsProcessing(String[] args) throws Exception {
    if (args.length > 2 || args.length < 2) {
      throw new Exception("le nombre de parametres est incorrecte");
    } else if (args[0] == null || args[0].isEmpty()) {
      throw new Exception("le path pour le fichier d'entree est vide");
    } else if (args[1] == null || args[1].isEmpty()) {
      throw new Exception("le path pour le fichier de sortie est vide");
    } else if (args[0].equals(args[1])) {
      throw new Exception("le chemin d'entree doit etre different de la sortie");
    }

    Path entryFileNamePath = Paths.get(args[0]);
    String entryFileName = entryFileNamePath.getFileName().toString();

    List<List<String>> csvLines = this.readFile(args[0]);
    List<Statistique> statistiques = this.processCSVFiles(csvLines, entryFileName);
    this.writeCSVFile(statistiques, args[1]);
    System.out.println("le programme a teminé avec succès !!");
  }
}
