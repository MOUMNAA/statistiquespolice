package com.uqam.PoliceInterventionStatistics;

import com.uqam.PoliceInterventionStatistics.service.CSVFileProcessor;

/**
 * Cette classe constitue le point d'entree de l'application et contient la fonction main pour
 * lancement de programme
 *
 * @author
 * @version
 */
public class Main {

  public static void main(String[] args) {

    try {
      CSVFileProcessor csvFileProcessor = new CSVFileProcessor();
      csvFileProcessor.startStatistcsProcessing(args);
    } catch (ArrayIndexOutOfBoundsException e) {
      System.out.println("priere de fournir les chemins des fichiers");
    } catch (Exception e) {
      System.out.println(e.getLocalizedMessage());
    }
  }
}
