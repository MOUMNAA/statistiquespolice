

Liste des exigences
===============
- TP1.1 : Les noms des fichiers d’entrée et de sortie sont reçus en paramètres au programme.
- TP1.2 : Les champs d'un fichier d'entrée doivent toujours être séparés par des virgules. (peu importe l'extension du fichier c'est le contenu qui compte).
- TP1.3 : Le fichier d'entrée ne doit pas être vide.
- TP1.4 : Les champs d'entête du fichier d'entrée doivent toujours être présents : `date, heure, parc, arrondissement, description.`
- TP1.5 : Le champ `date` doit être présent et doit être une chaine de caractères de la forme : `AAAA-MM-jj` suivant la norme `ISO8601`.
- TP1.6 : Le champ `heure` doit être présent et doit être une chaine de caractères de la forme : `HH:mm`.
- TP1.7 : Le champ `Parc` doit être présent et différent d'une chaine vide.
- TP1.8 : Le champ `arrondissement` doit être présent et figure sur le fichier `json` déposé au niveau de la racine.
- TP1.9 : Le champ `description` doit être présent et figure sur le fichier `json` déposé au niveau de la racine.
- TP1.10 : A la sortie, la liste des arrondissements devra être triée par ordre alphabétique.


Plan de tests
==========

| N.  | Fonctionnalité | Résultat attendu           | Description                   | Données  |                                                                                          
| :----: | :-------------- | :--------------------------- | :----------------------------- | :---------- |
|  C1   |   TP1.1 |  Message d'erreur et fin de programme     | Paramètres de programme vides | 2 paramètres vides (``)|                         
| C2  | TP1.1 | Message d'erreur    | Nombre de paramètres erroné     | 0 paramètres passés au programme |
| C3  | TP1.1  | Message d'erreur  | Nombre de paramètres erroné     | >2 paramètres passés au programme |
| C4  | TP1.1  | Message d'erreur  | Nombre de paramètres erroné     | <2 paramètres passés au programme |
| C5  | TP1.1   | Message d'erreur et fin de programme  |  Fichier non existant | 2 paramètres , fichier d'entrée n'existe pas |
| C6    | TP1.2 | Message d'erreur et fin de programme | Entrée erronée | 2 paramètres, les paramètres saisies sont égaux|
| C7    | TP1.2 | Message d'erreur et fin de programme | Entrée erronée | 2 paramètres, fichier d'entrée, données complètes séparées par des `'	'`|
| C8    | TP1.3 | Message d'erreur et fin de programme | Entrée erronée | 2 paramètres, fichier d'entrée vide|
|  C9  |  TP1.4| Message d'erreur | Champ manquant   | Données complètes, champ `date` manquant|
| C10    | TP1.4  | Message d'erreur | Champ manquant | Données complètes, champ `heure` manquant |
| C11    | TP1.4 | Message d'erreur | Champ manquant | Données complètes, champ `parc` manquant |
|  C12   | TP1.4 | Message d'erreur  | Champ manquant  | Données complètes, champ `arrondissement` manquant |
| C13   | TP1.4  | Message d'erreur| Champ manquant | Données complètes, champ `description` manquant |
|  C14   | TP1.5 | Message d'erreur et fin de programme| Champ erroné| 2 paramètres, Données complètes, format de date erroné |
| C15    | TP1.6 | Message d'erreur et fin de programme | Champ erroné  | 2 paramètres, Données complètes, format d'heure erroné
| C16    | TP1.7 | Message d'erreur et fin de programme  | Champ erroné | 2 paramètres, Données complètes, une colonne vide |
| C17    | TP1.8 | Message d'erreur et fin de programme  | Champ erroné | Fichier d'entrée contient un arrondissement qui ne figure pas sur le fichier `json`|
| C18    | TP1.9 | Message d'erreur et fin de programme   | Champ erroné |  Fichier d'entrée contient une description qui ne figure pas sur le fichier `json`|
| C19    | TP1.10 | Message de succès, génération de fichier dans le chemin de sortie et fin de programme   | Entrée correcte| Le fichier d'entrée respecte tous les exigences citées|
