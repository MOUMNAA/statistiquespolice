# Java Coding Style

* The `police-interventions-statistics` project is formatted using the
  [Google Java Style Guide](https://google.github.io/styleguide/javaguide.html).
* all classes and methods are written in english except JAVA doc is written in french language
